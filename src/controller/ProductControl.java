package controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.swing.JOptionPane;
import model.Product;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class ProductControl {

    private List<Product> products = new ArrayList<>();

    private String generateCode() {
        String code;
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        if (products.isEmpty()) {
            code = year + "1";
        } else {
        Product product = products.get(products.size() - 1);
        code = product.getCode().substring(4);
        int productNumber = Integer.parseInt(code) + 1;
        code = Integer.toString(year) + productNumber;
        }
        return code;
    }

    public void save(Product product) {
        product.setCode(generateCode());
        products.add(product);
        JOptionPane.showMessageDialog(null, "Produto, " + product.getName() + ", cadastrado com sucesso!");
    }
    
    public List<Product> getProducts() {
        return products;
    }
    
    public List<Product> searchByPrice(double price){
        List<Product> productsPrice = new ArrayList<>();
        for (Product product : products) {
            if (price >= product.getPrice()) {
                productsPrice.add(product);
            }
        }
        return productsPrice;
    }
}