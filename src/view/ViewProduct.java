package view;

import controller.ProductControl;
import java.util.List;
import javax.swing.JOptionPane;
import model.Product;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class ViewProduct {

    public static void main(String[] args) {
        String option;
        ProductControl pC = new ProductControl();
        List<Product> products;
        do {
            option = JOptionPane.showInputDialog("Digite a opção desejada:\n1) Cadastrar Produto:\n2) Listar Todos:\n3) Pesquisar produtos pelo valor:\n4) Sair");
            switch (option) {
                case "1":
                    pC.save(registerProduct());
                    break;
                case "2":
                    products = pC.getProducts();
                    listProduct(products);
                    break;
                case "3":
                    double price = Double.parseDouble(JOptionPane.showInputDialog("Pesquisar produtos até o valor de"));
                    products = pC.searchByPrice(price);
                    if (products.isEmpty()){
                        JOptionPane.showMessageDialog(null, "Nenhum produto nesse valor foi encontrado");
                    } else {
                        listProduct(products);
                    }
                    break;
                case "4":
                    System.exit(0);
                    break;
            }
        } while (!"4".equals(option));
    }

    private static Product registerProduct() {
        Product product = new Product();
        product.setName(JOptionPane.showInputDialog("Insira o nome do produto:"));
        product.setPrice(Double.parseDouble(JOptionPane.showInputDialog("Insira o valor do produto:")));
        product.setQuantity(Integer.parseInt(JOptionPane.showInputDialog("Insira a quantidade do produto:")));
        product.setDescription(JOptionPane.showInputDialog("Insira a descrição do produto:"));
        return product;
    }

    private static void listProduct(List<Product> products) {
        for (Product product : products) {
            System.out.println("Nome: " + product.getName());
            System.out.println("Código: " + product.getCode());
            System.out.println("Preço: " + product.getPrice());
            System.out.println("Quantidade: " + product.getQuantity());
            System.out.println("Descrição: " + product.getDescription());
            System.out.println("");
        }
        System.out.println("");
    }

}
